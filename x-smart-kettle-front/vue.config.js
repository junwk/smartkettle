const path = require("path");
var webpack = require("webpack");
const context = '/kettle-admin'
const resolve = dir => {
  return path.join(__dirname, dir);
};

const isProd = process.env.NODE_ENV === "production";

const isDev = process.env.NODE_ENV === 'development'

const assetsCDN = {
  // webpack build externals
  externals: {
    vue: "Vue",
    vuex: "Vuex",
    'vue-router': "VueRouter",
    axios: "axios",
    "view-design": "iview",
    "./cptable": "var cptable",
    "./jszip": "jszip",
  },
  css: [],
  // https://unpkg.com/browse/vue@2.6.10/
  js: [
    isProd ? context+"/npm/vue@2.6.10/dist/vue.min.js" :context+ "/npm/vue@2.6.10/dist/vue.js",
    context + '/npm/vue-router@3.5.3/dist/vue-router.min.js',
    context+ '/npm/vuex@3.0.1/dist/vuex.min.js',
    context+ '/npm/axios@0.18.0/dist/axios.min.js',
    context+'/npm/view-design@4.3.2/dist/iview.min.js'
  ]
};

module.exports = {
  // Project deployment base
  // By default we assume your app will be deployed at the root of a domain,
  // e.g. https://www.my-app.com/
  // If your app is deployed at a sub-path, you will need to specify that
  // sub-path here. For example, if your app is deployed at
  // https://www.foobar.com/my-app/
  // then change this to '/my-app/'
  publicPath: "/kettle-admin",
  // tweak internal webpack configuration.
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  // 如果你不需要使用eslint，把lintOnSave设为false即可
  lintOnSave: true,
  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src")) // key,value自行定义，比如.set('@@', resolve('src/components'))
      .set("_c", resolve("src/components"));

    if (isProd) {
      config.plugin('html').tap(args => {
        args[0].cdn = assetsCDN
        return args
      })
    }
    if (process.env.use_analyzer) {
      return {
        plugins: [
          // 使用包分析工具
          config
            .plugin('webpack-bundle-analyzer')
            .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
        ]
      }
    }
  },
  // 设为false打包时不生成.map文件
  productionSourceMap: false,
  configureWebpack: {
    plugins: [
      // (...)
      new webpack.IgnorePlugin(/cptable/)
    ],
    node: {
      fs: "empty"
    },
    externals: isProd ? assetsCDN.externals : [{
      './cptable': 'var cptable',
      './jszip': 'jszip'
    }]
  },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: ["./src/styles/variable.less"]
    }
  },
  css: {
    extract: process.env.NODE_ENV == "production" ? true : false,
    sourceMap: process.env.NODE_ENV == "development" ? true : false,
    loaderOptions: {
      less: {
        javascriptEnabled: true,
        paths: [
          path.resolve(__dirname, "src"),
          path.resolve(__dirname, "node_modules")
        ]
      }
    }
  },
  runtimeCompiler: isDev,
  devServer: {
    proxy: {
      "/xtl-server": {
         target: "http://localhost:9876/xtl-server/",
         // target: "http://101.132.24.211/xtl-server/",
        pathRewrite: { "^/xtl-server": "" },
        changeOrigin: true
     }
   }
  }
};
